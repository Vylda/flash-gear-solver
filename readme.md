
# Flash-gear.com puzzle solver

## Úvod

Pomůcka pro řešení puzzle ze serveru Flash-Gear.com

## Instalace

1. Nainstalovat Node.js (https://nodejs.org/en/download/)

2. Nainstalovat Chocolatey (správce balíčků) přes administrátorské okno PowerShelu
	```
	Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
	```

3. v témže okně spustit instalaci potřebných balíčků
	```
	choco install -y python2 gtk-runtime microsoft-build-tools libjpeg-turbo
	```

4.	Do složky C:/GTK nakopírovat GTK2 ze zip souboru (http://ftp.gnome.org/pub/GNOME/binaries/win32/gtk+/2.24/gtk+-bundle_2.24.10-20120208_win32.zip pro Win32 nebo http://ftp.gnome.org/pub/GNOME/binaries/win64/gtk+/2.22/gtk+-bundle_2.22.1-20101229_win64.zip pro Win64). __Neinstalovat GTK3, chybí knihovny!__


5. Nainstalovat Windows-Build-Tools (https://www.npmjs.com/package/windows-build-tools, stačí jen jednou); příkaz spustit v okně PowerShellu s administrátorskými právy
	```
	npm install --global --production windows-build-tools
	```

6. Vyklonovat repozitář
	* ``` git clone https://gitlab.com/Vylda/flash-gear-solver.git ```
	* ``` git clone git@gitlab.com:Vylda/flash-gear-solver.gitt ```
	
	nebo stáhnout

7. Ve složce repa (flash-gear-solver) spustit instalaci
	```
	npm install
	```
## Použití

1. Prvně je potřeba stáhnout flash soubor:

	Například flash http://five.flash-gear.com/npuz/puz.php?c=v&id=4756572&k=35244131&s=20
	Ve vývojářských nástrojích v záložce Network je třeba najít php soubor, který se nejdéle načítá. Ten je třeba otevřít v novém panelu (při načítání dílků se neobjeví se loader!). Flash pak uložit na disk (obvykle puz.php.swf).

2. Nastavit parametry puzzle v souboru index.js:

	```javascript
	let options = {
		swf: "./puz.php.swf" //relativní cesta k swf souboru
	};
	```

	Předvolby mohou mít i mnepovinné položky dir, rows, cols a tileSize (budete na ně v průběhu zpracování dotázáni):
	```javascript
	let options = {
		swf: "./puz.php.swf", //relativní cesta k swf souboru
		dir: "./images/", //složka pro extrahované soubory
		cols: 7, //počet řádků v puzzle
		rows: 5, //počet sloupců v puzzle
		tileSize: 180 //velikost extrahovaného obrázku ze souboru swf
	};
	```

	Pokud chcete použít výše uvedené přednastavení, nechte options prázdné:
	
	```javascript
	let options = {};
	```

4. spustit sloučení
	```
	npm run extract
	```

	Dále postupujte podle pokynů.

5. Sloučené dílky se uloží do souboru puzzle.png

