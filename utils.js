let fs = require("fs");
let prompt = require('prompt');
let colors = require("colors/safe");

module.exports = {
	deleteFolderRecursive(path) {
		if (fs.existsSync(path)) {
			fs.readdirSync(path).forEach(function(file, index) {
				var curPath = path + "/" + file;
				if (fs.lstatSync(curPath).isDirectory()) { // recurse
					deleteFolderRecursive(curPath);
				} else { // delete file
					fs.unlinkSync(curPath);
				}
			});
			try {
				fs.rmdirSync(path);
				console.info(colors.gray("Folder " + path + " deleted!"));
			} catch (err) {
				console.error(colors.red("Folder " + path + " delete problem!"));
			}
		}
	},

	askNumber(q, def) {
		prompt.message = colors.green("Question");
		prompt.delimiter = colors.green(": ");
		let schema = {
			properties: {
				number: {
					description: colors.green(q),
					pattern: /^\d+$/,
					message: colors.red('You must type number (integer)!'),
					required: true
				}
			}
		};
		if (def) {
			schema.properties.number.default = def;
		}

		return new Promise((resolve) => {
			prompt.get(schema, (err, result) => {
				resolve(err ? def : result.number);
			});
		})
	},

	getColsRows(options, sum) {
		let rows = options.rows,
			cols = options.cols,
			size = options.tileSize;
		return new Promise((resolve, reject) => {
			return this.askNumber(`Size of an image in ${options.folder} folder (in px)?`, size).then(xsize => {
				size = xsize;
				return this.askNumber(`Columns of puzzle?`, options.cols).then(xcols => {
					cols = xcols;
					return this.askNumber(`Rows of puzzle?`, options.rows).then(xrows => {
						rows = xrows;
						if (rows * cols == sum) {
							return resolve({ cols, rows, size});
						} else {
							console.log(colors.red(`Rows (${rows}) × columns (${cols}) is not ${sum}!`));
							return reject();
						}
					});
				});
			})
			
		});
	}
}
