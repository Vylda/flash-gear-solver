"use strict"

let Extractor = require("swf-image-extractor");
const utils = require("./utils");
let fs = require("fs");
let colors = require("colors/safe");

class MyExtractor {
	constructor(file, folder) {
		this._folder = folder
		utils.deleteFolderRecursive(folder);
		this._file = file;
		fs.mkdirSync(folder);
	}

	extract() {
		console.info(colors.gray(`Images extract from ${this._file} to ${this._folder} started!`));
		return Extractor.getImageTags(fs.openSync(`${this._file}`, 'r'))
		.then((imageTags, jpegTablesTag) => {
			for (let tag of imageTags) {
				let newTag = this._removeSecondSoi(tag);
				Extractor.getImageStream(newTag, jpegTablesTag)
					.pipe(fs.createWriteStream(`${this._folder}${tag.characterId}.${tag.filetype}`));
			}
			console.info(colors.gray(`Images extract from ${this._file} to ${this._folder} completed!`));
			Promise.resolve();
		}).catch((err) => {
			throw new Error(err);
		});
	}
	_removeSecondSoi(tag) {
		let newTag = Object.assign({imageData: null}, tag);
		let imageData = tag.imageData	
		if (imageData[0] == 255 && tag.imageData[1] == 217 && tag.imageData[2] == 255 && tag.imageData[3] == 216 && imageData[4] == 255 && tag.imageData[5]) {
			imageData = imageData.slice(4);
		}
		newTag.imageData = imageData;
		return newTag;
	}
}
module.exports = MyExtractor;
