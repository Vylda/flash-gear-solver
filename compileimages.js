"use strict"

const fs = require('fs');
const mergeImages = require('merge-images');
const Canvas = require('canvas');
let SwfExtractor = require("./extractimages");
const utils = require("./utils");
let colors = require("colors/safe");

class MergeImageFromFolder {
	constructor(options = {}) {
		this._options = Object.assign(options, {
			swf: "./puz.php.swf",
			dir: "./images/",
			cols: 7,
			rows: 5,
			tileSize: 180
		});

		this._imageNames = [];
		let ext;

		if (!options.noExtract) {
			ext = new SwfExtractor(this._options.swf, this._options.dir);
		} else {
			ext = {};
			ext.extract = () => {
				return Promise.resolve();
			}
		}


		ext.extract().then(() => {
			let p = this._readImages(this._options.dir);
			p.then(() => {
				console.info(colors.gray("Reading images completed!"));
				utils.getColsRows(this._options, this._imageNames.length).then(data => {
					this._options.rows = data.rows;
					this._options.cols = data.cols;
					this._options.tileSize = data.tileSize;
					this._mergeImages();
				}).catch(() => {
					console.info(colors.yellow("Rerun 'npm run solve'!"));
				});
			}).catch(err => {
				console.error(colors.red(err.message));
				switch (err.type) {
					case "png":
						console.info(colors.yellow(`You must convert jpeg images in ${this._options.dir} to PNG and run 'npm run solve'!`));
						break;
				}

			});
		});

	}

	_readImages(folder) {
		console.info(colors.gray("Reading images started!"));
		this._imageNames = [];
		return new Promise((resolve, reject) => {
			if (!fs.existsSync(folder)) {
				reject({ message: `Folder ${folder} doesn't exists!` });
				return;
			}
			fs.readdir(folder, (err, filenames) => {
				if (err) {
					throw new Error(err);
				}
				filenames.forEach(filename => {
					if (filename.endsWith(".png")) {
						let file = this._options.dir + filename;
						this._imageNames.push(file);
					}
				});
				if (this._imageNames.length) {
					resolve();
				} else {
					reject({
						message: "No extracted png images is found!",
						type: "png"
					});
				}
			});
		});
	}

	_mergeImages() {
		console.info(colors.gray("Images to puzzle merge started!"));
		console.log(`Merging ${this._imageNames.length} pieces.`);

		let tileObjects = [];
		let tileIndex = 0;
		for (let y = 0; y < this._options.rows; y++) {
			for (let x = 0; x < this._options.cols; x++) {
				if (this._imageNames[tileIndex]) {
					tileObjects.push({
						src: this._imageNames[tileIndex],
						x: x * this._options.tileSize / 2,
						y: y * this._options.tileSize / 2
					});
				}
				tileIndex++;
			}
		}

		let opt = {
			Canvas: Canvas,
			width: (this._options.cols / 2 + 0.5) * this._options.tileSize,
			height: (this._options.rows / 2 + 0.5) * this._options.tileSize
		};

		mergeImages(tileObjects, opt).then(b64 => {
			let base = b64.replace(/^data:image\/png;base64,/, "");
			fs.writeFile("puzzle.png", base, 'base64', err => {
				if (!err) {
					console.info(colors.cyan("Puzzle was merged!"));
				} else {
					console.error(err);
				}
			});
			utils.deleteFolderRecursive(this._options.dir);
		}).catch(err => {
			console.log("error", err)
		});
	}

}

module.exports = MergeImageFromFolder;
