"use strict"

let Miff = require("./compileimages");

let options = {
	cols: 5,
	rows: 7,
	tileSize: 180,
	swf: "./puz.php.swf"
}


if (process.argv[2] && process.argv[2] == '-converted') {
	options.noExtract = true;
}
new Miff(options);